import { TestBed } from '@angular/core/testing';

import { PointCRUDService } from './point-crud.service';

describe('PointCRUDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PointCRUDService = TestBed.get(PointCRUDService);
    expect(service).toBeTruthy();
  });
});
