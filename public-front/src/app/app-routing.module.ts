import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilPublicComponent } from './pages/accueil-public/accueil-public.component';
import { TableScoreCompletComponent } from './pages/table-score-complet/table-score-complet.component';
import { TableScoreMatchComponent } from './pages/table-score-match/table-score-match.component';


const routes: Routes = [
  { path: 'acceuil', component: AccueilPublicComponent },
  { path: 'tableScore',        component: TableScoreCompletComponent },
  { path: 'tableScoreMatch/:id/:j1/:j2', component: TableScoreMatchComponent },
  { path: '',   redirectTo: '/acceuil', pathMatch: 'full' },
  { path: '**', component: AccueilPublicComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
