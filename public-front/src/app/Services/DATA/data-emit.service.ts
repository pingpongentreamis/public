import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataEmitService {

  private dataSource = new BehaviorSubject('default data');
  currentMessage = this.dataSource.asObservable();

  constructor() { }

  changeMessage(data: string) {
    this.dataSource.next(data);
  }
}
