import { TestBed } from '@angular/core/testing';

import { DataEmitService } from './data-emit.service';

describe('DataEmitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataEmitService = TestBed.get(DataEmitService);
    expect(service).toBeTruthy();
  });
});
