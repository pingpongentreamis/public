import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { handleError } from '../../Function-Utils/LogError';
import { SportModele } from '../../Modele/SportModele';
import { environment } from 'src/environments/environment.prod';

interface TabSport {
  sport: Array<SportModele>;
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root'
})
export class SportCRUDService {
  url = environment.url
  constructor(private http: HttpClient) { }

/**
 * getSports
 */
getSports(): Observable<TabSport> {
  return  this.http.get<TabSport>(this.url + '/Sports/').pipe(
    tap(_ => console.log(`fetched Sports `)),
    catchError(handleError<TabSport>(`getSports`))
  );
}

/**
 * getSportById
 * Recupère une Sports avec un id
 * @param id - id of the Sports
 */
getSportById(id: number): Observable<SportModele> {
  return  this.http.get<SportModele>(this.url + '/Sport/' + id).pipe(
    tap(_ => console.log(`fetched Sport id=${id}`)),
    catchError(handleError<SportModele>(`getSportById id=${id}`))
  );
}
}
