import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { handleError } from '../../Function-Utils/LogError';
import { PointModele, PointScore } from '../../Modele/PointModele';
import { environment } from 'src/environments/environment.prod';

interface TabPoint {
  point: Array<PointModele>;
}



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PointCRUDService {
  url = environment.url

  constructor(private http: HttpClient) { }


/**
 * getPoints
 */
getPoints(): Observable<TabPoint> {
  return  this.http.get<TabPoint>(this.url + '/Points/').pipe(
    tap(_ => console.log(`fetched Points `)),
    catchError(handleError<TabPoint>(`getPoints`))
  );
}

/**
 * getPointById
 * Recupère une Point avec un id
 * @param id - id of the Point
 */
getPointById(id: number): Observable<TabPoint> {
  return  this.http.get<TabPoint>(this.url + '/Point/' + id).pipe(
    tap(_ => console.log(`fetched Point id=${id}`)),
    catchError(handleError<TabPoint>(`getPointById id=${id}`))
  );
}

/**
 * insertPoint
 * Insert une Point en base de données
 * @param point - object Point
 */
insertPoint(point: PointModele) {
  return this.http.post<PointModele>(this.url + '/Point/', point , httpOptions).pipe(
    tap((point: PointModele) => console.log(`added point`)),
    catchError(handleError<PointModele>('insert point'))
  );
}

/**
 * getPointForPlayerByPartie
 * @param id - id Player
  * @param partie - id partie
 */
getPointForPlayerByPartie(id: number, partie: number) {
  return this.http.get<PointScore>(this.url + '/PointsByJoueurByPartie/' + partie + '/' + id);
}

/**
 * deletePoint
 * Suprime une Point en base de données
 * @param id - id of the Point
 */
deletePoint(id: number) {
  return this.http.delete<PointModele>(this.url + '/Point/' + id , httpOptions).pipe(
    tap((Point: PointModele) => console.log(`delete Point`, id, Point)),
    catchError(handleError<PointModele>('delete   Point'))
  );
  }
}
