import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BanniereComponent } from './components/banniere/banniere.component';
import { TableScoreCompletComponent } from './pages/table-score-complet/table-score-complet.component';
import { AccueilPublicComponent } from './pages/accueil-public/accueil-public.component';
import { TableScoreMatchComponent } from './pages/table-score-match/table-score-match.component';

import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PartieCRUDService } from './Services/API-CALL/partie-crud.service';
import { PointCRUDService } from './Services/API-CALL/point-crud.service';
import { SportCRUDService } from './Services/API-CALL/sport-crud.service';
import { DataEmitService } from './Services/DATA/data-emit.service';

@NgModule({
  declarations: [
    AppComponent,
    BanniereComponent,
    TableScoreCompletComponent,
    AccueilPublicComponent,
    TableScoreMatchComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatButtonModule,
    NgSelectModule,
    FormsModule,
    MatChipsModule,
    AppRoutingModule,
    HttpClientModule,
    MatIconModule,
    MatSelectModule,
    MatTableModule
  ],
  providers: [PartieCRUDService,
      PointCRUDService,
    SportCRUDService,
    DataEmitService],
  bootstrap: [AppComponent]
})
export class AppModule { }
