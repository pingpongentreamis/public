import { TestBed } from '@angular/core/testing';

import { PartieCRUDService } from './partie-crud.service';

describe('PartieCRUDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PartieCRUDService = TestBed.get(PartieCRUDService);
    expect(service).toBeTruthy();
  });
});
