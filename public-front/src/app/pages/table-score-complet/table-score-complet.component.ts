import { Component, OnInit, Input } from '@angular/core';
import { PartieCRUDService } from '../../Services/API-CALL/partie-crud.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { PointCRUDService } from 'src/app/Services/API-CALL/point-crud.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-table-score-complet',
  templateUrl: './table-score-complet.component.html',
  styleUrls: ['./table-score-complet.component.scss']
})
export class TableScoreCompletComponent implements OnInit {

  @Input() datatest$: Observable<any>;
  // datatest$: any;
  score: any;
  sub: Subscription;

  constructor(private partie: PartieCRUDService, private point: PointCRUDService) {
  }

  ngOnInit() {

    
    this.datatest$ = Observable.interval(10000).startWith(0).switchMap(() => this.partie.getPartie());
  }
}
