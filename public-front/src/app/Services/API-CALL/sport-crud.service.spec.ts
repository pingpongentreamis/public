import { TestBed } from '@angular/core/testing';

import { SportCRUDService } from './sport-crud.service';

describe('SportCRUDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SportCRUDService = TestBed.get(SportCRUDService);
    expect(service).toBeTruthy();
  });
});
