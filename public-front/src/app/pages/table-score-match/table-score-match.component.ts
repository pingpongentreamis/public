import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { PointCRUDService } from 'src/app/Services/API-CALL/point-crud.service';
import { PartieCRUDService } from 'src/app/Services/API-CALL/partie-crud.service';
@Component({
  selector: 'app-table-score-match',
  templateUrl: './table-score-match.component.html',
  styleUrls: ['./table-score-match.component.scss']
})

export class TableScoreMatchComponent implements OnInit {

  id;
  nomJoueur1: string = '';
  nomJoueur2: string = '';
  index: number = 1;
  @Input()j: number = 0;
  pointJ1: number = 0;
  pointJ2: number = 0;
  @Input()j1;
  @Input()j2;
  @Input() datatest$: Observable<any>;

  constructor(private router: Router, private route: ActivatedRoute, private point: PointCRUDService, private partie: PartieCRUDService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.j1 = this.route.snapshot.paramMap.get('j1').toString();
    this.j2 = this.route.snapshot.paramMap.get('j2').toString();
   }

  ngOnInit() {
      this.partie.getPartieById(this.id).subscribe( data => {
        console.log(data);
        this.nomJoueur1 = data.joueur1.Nom;
        this.nomJoueur2 = data.joueur2.Nom;
      });
      this.point.getPointForPlayerByPartie(this.j1, this.id).subscribe( data => this.pointJ1 = data.totalPoints);
      this.point.getPointForPlayerByPartie(this.j2, this.id).subscribe( data => this.pointJ2 = data.totalPoints);
      this.datatest$ = Observable.interval(10000).startWith(0).switchMap(() => this.point.getPointById(this.id));

  }

}
