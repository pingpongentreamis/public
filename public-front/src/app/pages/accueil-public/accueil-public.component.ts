import { Component, OnInit, Input } from '@angular/core';
import { PartieCRUDService } from 'src/app/Services/API-CALL/partie-crud.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';
import { DataEmitService } from 'src/app/Services/DATA/data-emit.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accueil-public',
  templateUrl: './accueil-public.component.html',
  styleUrls: ['./accueil-public.component.scss']
})
export class AccueilPublicComponent implements OnInit {

  matchs: any;
  sub: Subscription;
  selectedPersonId;
  message;
  @Input() datatest$: Observable<any>;

  constructor(private partie: PartieCRUDService, private data: DataEmitService, private router: Router) { }

  ngOnInit() {
    Observable.interval(1000*60).startWith(0).switchMap(() => this.partie.getPartie()).subscribe( data => this.matchs = data);

  }

  getAllMacth() {
    console.log("getAllMacth");
    this.router.navigate(['/tableScore']);

  }

  getMacth() {
      console.log("getMacth", this.selectedPersonId);
      this.partie.getPartieById(this.selectedPersonId).subscribe( data => {
        console.log(data.joueur2.ID);
        
        this.router.navigate(['/tableScoreMatch/', this.selectedPersonId, data.joueur1.ID, data.joueur2.ID]);
      });
      

  }

}
